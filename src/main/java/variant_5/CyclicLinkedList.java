package variant_5;

public class CyclicLinkedList<T> {
    private Element<T> head = null;
    private Element<T> tail = null;

    public CyclicLinkedList() {
    }

    /**
     * Добавляет элемент в конец списка
     */
    void addInEnd(T value) {
        Element<T> newElem = new Element<>(value);
        newElem.next = null;
        if (head == null) {
            head = tail = newElem;
            tail.next = head;
        } else {
            tail.next = newElem;
            newElem.next = head;
            tail = newElem;
        }
    }

    /**
     * Добавляет элемент в начало списка
     */
    void addInBeginning(T value) {
        Element<T> newElem = new Element<>(value);
        if (head == null) {
            head = tail = newElem;
            head.next = head;
        } else {
            tail.next = newElem;
            newElem.next = head;
            head = null;
        }
    }

    /**
     * Возвращает количество элементов
     */
    int getElementsNumber() {
        int counter = 0;
        Element<T> temp = head;
        while (temp != tail) {
            temp = temp.next;
            counter++;
        }
        return counter == 0 ? counter : ++counter;
    }

    /**
     * Удаляет список
     */
    void deleteList() {
        head = tail = null;
    }

    /**
     * Удаляет первый элемент
     */
    public T deleteFirst() {
        if (head != null) {
            T value = head.value;
            head = head.next;
            tail.next = head;
            return value;
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty cyclic linked list!");
        }
    }

    /**
     * Удаляет последний элемент
     */
    public T deleteLast() {
        Element<T> temp = head;
        if (head != null) {
            if (temp.next == head) {
                // элемент единственный
                T value = head.value;
                head = tail = null;
                return value;
            } else {
                while (temp.next != tail) {
                    temp = temp.next;
                }
                T value = temp.next.value;
                temp.next = head;
                tail = temp;
                return value;
            }
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty cyclic linked list!");
        }
    }

    /**
     * Проверка пустоты списка
     */
    boolean isEmpty() {
        return head == null;
    }

    private class Element<T> {
        Element(T value) {
            this.value = value;
        }

        Element<T> next;
        T value;
    }
}
