package variant_1;

// <-> (*head)x1 <- x2 <- x3(*tail) <->
public class Deque<T> {
    private Element<T> head = null;

    private Element<T> tail = null;

    public Deque() {
    }

    /**
     * Добавляет элемент в начало дека
     */
    void offerFirst(T value) {
        if (head == null) {
            head = new Element<>(value);
            head.next = null;
            tail = head;
        } else {
            // добавляем элементы в head - начало очереди
            Element<T> newNode = new Element<>(value);
            newNode.next = null;
            head.next = newNode;
            head = newNode;
        }
    }

    /**
     * Добавляет элемент в конец дека
     */
    void offerLast(T value) {
        if (head == null) {
            head = new Element<>(value);
            head.next = null;
            tail = null;
        } else {
            // добавляем элементы в tail - конец очереди
            Element<T> newNode = new Element<>(value);
            newNode.next = tail;
            tail = newNode;
        }
    }

    /**
     * Удаляет элемент из начала дека
     */
    T pollFirst() {
        if (head!= null && head == tail) {
            T value = head.value;
            head = null;
            return value;
        } else if (head != null) {
            Element<T> temp = tail;
            while (temp.next != head) {
                temp = temp.next;
            }
            Element<T> proxy = head;
            head = temp;
            head.next = null;
            T value = proxy.value;
            proxy = null;
            return value;
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty deque!");
        }
    }

    /**
     * Удаляет элемент из конца дека
     */
    T pollLast() {
        if (head!= null && head == tail) {
            T value = head.value;
            head = null;
            return value;
        } else if (head != null) {
            Element<T> proxy = tail;
            tail = tail.next;
            T value = proxy.value;
            proxy = null;
            return value;
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty deque!");
        }
    }

    /**
     * Проверка на пустоту
     */
    boolean isEmpty() {
        return head == null;
    }

    private class Element<T> {
        Element(T value) {
            this.value = value;
        }

        Element<T> next;
        T value;
    }
}
