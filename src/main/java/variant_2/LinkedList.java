package variant_2;

// -> (*head)x1 -> x2 -> x3 -> null
public class LinkedList<T> {
    private Element<T> head = null;

    public LinkedList() {
    }

    /**
     * Добавляет элемент в конец списка
     */
    void addInEnd(T value) {
        Element<T> newElem = new Element<>(value);
        newElem.next = null;
        if (head == null) {
            head = newElem;
        } else {
            Element<T> temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = newElem;
        }
    }

    /**
     * Добавляет элемент в начало списка
     */
    void addInBeginning(T value) {
        Element<T> newElem = new Element<>(value);
        if (head == null) {
            head = newElem;
        } else {
            newElem.next = head;
            head = null;
        }
    }

    /**
     * Возвращает количество элементов
     */
    int getElementsNumber() {
        int counter = 0;
        Element<T> temp = head;
        while (temp != null) {
            temp = temp.next;
            counter++;
        }
        return counter;
    }

    /**
     * Удаляет список
     */
    void deleteList() {
        head = null;
    }

    /**
     * Удаляет первый элемент
     */
    public T deleteFirst() {
        if (head != null) {
            T value = head.value;
            head = head.next;
            return value;
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty linked list!");
        }
    }

    /**
     * Удаляет последний элемент
     */
    public T deleteLast() {
        Element<T> temp = head;
        if (head != null) {
            if (temp.next == null){
                // элемент единственный
                T value = head.value;
                head = null;
                return value;
            } else {
                while (temp.next.next != null) {
                    temp = head.next;
                }
                T value = temp.next.value;
                temp.next = null;
                return value;
            }
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty linked list!");
        }
    }

    /**
     * Проверка пустоты списка
     */
    boolean isEmpty() {
        return head == null;
    }

    private class Element<T> {
        Element(T value) {
            this.value = value;
        }

        Element<T> next;
        T value;
    }
}
