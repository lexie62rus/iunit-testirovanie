package variant_4;

//  <> (*head) x1 <-> x2 <-> x3 (*tail) <> x1 ... и т.д.
public class BiconnectedCyclicLinkedList<T> {
    private Element<T> head = null;
    private Element<T> tail = null;

    public BiconnectedCyclicLinkedList() {
    }

    /**
     * Добавляет элемент в конец списка
     */
    void addInEnd(T value) {
        Element<T> newElem = new Element<>(value);
        if (head == null) {
            head = tail = newElem;
            head.next = head;
            head.prev = head;
        } else {
            tail.next = newElem;
            newElem.prev = tail;
            tail = newElem;
            newElem.next = head;
            head.prev = tail;
        }
    }

    /**
     * Добавляет элемент в начало списка
     */
    void addInBeginning(T value) {
        Element<T> newElem = new Element<>(value);
        if (head == null) {
            head = tail = newElem;
            head.next = head;
            head.prev = head;
        } else {
            newElem.next = head;
            newElem.prev = tail;
            head.prev = newElem;
            tail.next = newElem;
            head = null;
        }
    }

    /**
     * Возвращает количество элементов
     */
    int getElementsNumber() {
        int counter = 0;
        Element<T> temp = head;
        while (temp != tail) {
            temp = temp.next;
            counter++;
        }
        return counter == 0 ? counter : ++counter;
    }

    /**
     * Удаляет список
     */
    void deleteList() {
        head = null;
        tail = null;
    }

    /**
     * Удаляет первый элемент
     */
    public T deleteFirst() {
        if (head != null) {
            T value = head.value;
            if (head.next == head) {
                // элемент первый
                head = tail = null;
            } else {
                tail.next = head.next;
                head.prev = tail;
                head = head.next;
            }
            return value;
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty biconnected cyclic linked list!");
        }
    }

    /**
     * Удаляет последний элемент
     */
    public T deleteLast() {
        if (head != null) {
            T value = tail.value;
            if (head.next == head) {
                // элемент единственный
                head = tail = null;
            } else {
                tail.prev.next = head;
                head.prev = tail.prev;
                tail = tail.prev;
            }
            return value;
        } else {
            throw new NullPointerException("You're trying to " +
                    "get element from empty biconnected cyclic linked list!!");
        }
    }

    /**
     * Проверка пустоты списка
     */
    boolean isEmpty() {
        return head == null;
    }

    private class Element<T> {
        Element<T> next;
        Element<T> prev;
        T value;

        Element(T value) {
            this.value = value;
        }
    }
}
