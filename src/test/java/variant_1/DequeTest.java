package variant_1;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class DequeTest {
    /**
     * -- Часть 1. Готовые тесты.
     **/
    @Test
    public void test_offerFirst() {
        Deque<Character> deque = new Deque<>();
        deque.offerFirst('6');
        assertTrue(deque.isEmpty());
    }

    @Test
    public void test_offerLast() {
        Deque<Character> deque = new Deque<>();
        deque.offerLast('6');
        assertFalse(deque.isEmpty());
    }

    /**
     * -- Часть 2. Напишите свои после этой строки.
     **/
}
