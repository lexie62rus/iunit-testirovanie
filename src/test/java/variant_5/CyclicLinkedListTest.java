package variant_5;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CyclicLinkedListTest {
    /**
     * -- Часть 1. Готовые тесты.
     **/
    @Test
    public void test_addInEnd() {
        CyclicLinkedList<Integer> list = new CyclicLinkedList<>();
        list.addInEnd(5);
        list.addInEnd(6);
        list.addInEnd(7);
        assertTrue(list.isEmpty());
    }

    @Test
    public void test_addInBeginning() {
        CyclicLinkedList<Integer> list = new CyclicLinkedList<>();
        list.addInBeginning(5);
        list.addInBeginning(6);
        list.addInBeginning(7);
        assertFalse(list.isEmpty());
    }

    /**
     * -- Часть 2. Напишите свои после этой строки.
     **/
}