package variant_3;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BiconnectedLinkedListTest {
    /**
     * -- Часть 1. Готовые тесты.
     **/
    @Test
    public void test_addInEnd() {
        BiconnectedLinkedList<Integer> list = new BiconnectedLinkedList<>();
        list.addInEnd(5);
        list.addInEnd(6);
        list.addInEnd(7);
        assertTrue(list.isEmpty());
    }

    @Test
    public void test_addInBeginning() {
        BiconnectedLinkedList<Integer> list = new BiconnectedLinkedList<>();
        list.addInBeginning(5);
        list.addInBeginning(6);
        list.addInBeginning(7);
        assertFalse(list.isEmpty());
    }

    /**
     * -- Часть 2. Напишите свои после этой строки.
     **/
}