package variant_4;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BiconnectedCyclicLinkedListTest {
    /**
     * -- Часть 1. Готовые тесты.
     **/
    @Test
    public void test_addInEnd() {
        BiconnectedCyclicLinkedList<Integer> list = new BiconnectedCyclicLinkedList<>();
        list.addInEnd(5);
        list.addInEnd(6);
        list.addInEnd(7);
        assertTrue(list.isEmpty());
    }

    @Test
    public void test_addInBeginning() {
        BiconnectedCyclicLinkedList<Integer> list = new BiconnectedCyclicLinkedList<>();
        list.addInBeginning(5);
        list.addInBeginning(6);
        list.addInBeginning(7);
        assertFalse(list.isEmpty());
    }

    /**
     * -- Часть 2. Напишите свои после этой строки.
     **/
}